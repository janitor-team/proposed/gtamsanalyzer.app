//
//  MWFakeFile.m
//  TEST2
//
//  Created by matthew on Thu Jun 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MWFakeFile.h"


@implementation MWFakeFile
-(id) initWithString: (NSString *) what
{
    self = [super init];
    myString = [[NSString alloc] initWithString: what];
    [path setString: @""];
    return self;
}

-(void) dealloc
{
    [myString release];
    [super dealloc];
}

-(NSString *) name {return path;}
-(NSString *) path {return path;}
-(NSString *) string {return myString;}
-(BOOL) open {return YES;}
-(void) setOpen: (BOOL) state {;}
-(void) setOpenFile: (id) aFid
{;}
-(void) setHasFile: (BOOL) state {;}
-(BOOL) hasFile {return NO;}

@end
