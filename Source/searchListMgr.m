#import "searchListMgr.h"
#import "myProject.h"

@implementation searchListMgr
- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
    return [[myProj searchList] count];

}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
   return [[[myProj searchList] objectAtIndex: rowIndex] name];
}
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
    return NO;
}
@end
