#import "codeListSearchWatcher.h"
#import "myProject.h"
@implementation codeListSearchWatcher
-(BOOL)tableView: (NSTableView *) aTable shouldEditTableColumn: (NSTableColumn *) aCol row: (unsigned) arow
{
    return NO;
}
-(IBAction) doRefresh: (id) sender
{
    [myOwnCodeList reloadData];
}
- (void)tabView:(NSTabView *)tabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
    if([[tabViewItem label] isEqualToString: @"Search"])
    {
	[myOwnCodeList reloadData];
	[myOwnCodeList setTarget: self];
	[myOwnCodeList setDoubleAction: @selector(displaySelData)];
    }

}
-(void) displaySelData
{
    int row;
    NSMutableString *ss;
    
    ss = [[NSMutableString alloc] init];
    [ss setString: [searchString stringValue]];
    row = [myOwnCodeList selectedRow];
    [ss appendString: [[(myProject *)[myOwner getGWorkBench] hotCodeList] objectAtIndex: row]];
    [searchString setStringValue: ss];
    //get the selected row
/*    tt =  [[myOwner hotCodeList] objectAtIndex: row];
    row = [theResults selectedRow];
    //get the appropriate data
    if(row >= 0)
    {
	ss = [[myData objectAtIndex: row] objectForKey: @"_data"];
    //set it to the textview
	[thisCell setString: ss];
    }*/
}
-(IBAction) clearSearch: (id) Sender
{
    [searchString setStringValue: @""];
}
@end
