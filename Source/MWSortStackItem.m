//
//  MWSortStackItem.m
//  TamsAnalyzer
//
//  Created by matthew on Sat Mar 08 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MWSortStackItem.h"
#import "utils.h"
#import "tamsutils.h"

int stackComp(id first, id second, NSMutableArray *sortStack)
{
    int i;
    int cnt;
    NSMutableDictionary *thisOne;
    NSComparisonResult cr;
    cr = NSOrderedAscending;
    cnt = [sortStack count];
    for(i = 0; i < [sortStack count]; i++)
    {
	thisOne = [sortStack objectAtIndex: i];
        if([[thisOne objectForKey: @"type"] intValue] == SORTSTRING)
	{
            if([thisOne objectForKey: @"case"] == nil)
                cr = [(NSString *) [first objectForKey: [thisOne objectForKey: @"col"]] compare:
                        [second objectForKey: [thisOne objectForKey: @"col"]]];
            else
                cr = [(NSString *) [first objectForKey: [thisOne objectForKey: @"col"]] compare:
                                  [second objectForKey: [thisOne objectForKey: @"col"]] options: NSCaseInsensitiveSearch];
	    if(cr == NSOrderedSame) continue;
            if([[thisOne objectForKey: @"up"] boolValue] == YES)
	    {
		if(cr == NSOrderedAscending) return NSOrderedAscending;
		if(cr == NSOrderedDescending) return NSOrderedDescending;
	    }
	    else
	    {
		if(cr == NSOrderedAscending) return NSOrderedDescending;
		if(cr == NSOrderedDescending) return NSOrderedAscending;
	    }
	    
	}
	else if([[thisOne objectForKey: @"type"] intValue] == SORTINTNUMBER)
	{
	    if([[first objectForKey: [thisOne objectForKey: @"col"] ] intValue] < 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] intValue])
		cr = -1;
	    if([[first objectForKey: [thisOne objectForKey: @"col"] ] intValue] > 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] intValue])
		cr = 1;
	     if([[first objectForKey: [thisOne objectForKey: @"col"] ] intValue] == 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] intValue])
		cr = 0;
	    if(cr == 0) continue;
	    if([[thisOne objectForKey: @"up"] boolValue] == YES)
	    {
		if(cr == -1) return NSOrderedAscending;
		if(cr == 1) return NSOrderedDescending;
	    }
	    else
	    {
		if(cr == -1) return NSOrderedDescending;
		if(cr == 1) return NSOrderedAscending;
	    }
	    
	
	}
	else if([[thisOne objectForKey: @"type"] intValue] == SORTFLOATNUMBER)
	{
	    if([[first objectForKey: [thisOne objectForKey: @"col"] ] doubleValue] < 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] doubleValue])
		cr = -1;
	    if([[first objectForKey: [thisOne objectForKey: @"col"] ] doubleValue] > 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] doubleValue])
		cr = 1;
	     if([[first objectForKey: [thisOne objectForKey: @"col"] ] doubleValue] == 
		[[second objectForKey: [thisOne objectForKey: @"col"] ] doubleValue])
		cr = 0;
	    if(cr == 0) continue;
	    if([[thisOne objectForKey: @"up"] boolValue] == YES)
	    {
		if(cr == -1) return NSOrderedAscending;
		if(cr == 1) return NSOrderedDescending;
	    }
	    else
	    {
		if(cr == -1) return NSOrderedDescending;
		if(cr == 1) return NSOrderedAscending;
	    }
	    
	
	}
 	else if([[thisOne objectForKey: @"type"] intValue] == SORTDATE)
	{
	    if([(NSDate *)[NSCalendarDate dateWithString: [first objectForKey: [thisOne objectForKey: @"col"] ]
		calendarFormat: [thisOne objectForKey: @"dateFormat"] ] compare: 
		(NSDate *)[NSCalendarDate dateWithString: [second objectForKey: [thisOne objectForKey: @"col"] ]
		calendarFormat: [thisOne objectForKey: @"dateFormat"] ]] == NSOrderedDescending)
		cr = -1;
		
	   else if([(NSDate *)[NSCalendarDate dateWithString: [first objectForKey: [thisOne objectForKey: @"col"] ]
		calendarFormat: [thisOne objectForKey: @"dateFormat"] ] compare: 
		(NSDate *)[NSCalendarDate dateWithString: [second objectForKey: [thisOne objectForKey: @"col"] ]
		calendarFormat: [thisOne objectForKey: @"dateFormat"] ]] == NSOrderedAscending)
		cr = 1;
	    else continue;
	    
	    
	    if([[thisOne objectForKey: @"up"] boolValue] == YES)
	    {
		if(cr == 1) return NSOrderedAscending;
		if(cr == -1) return NSOrderedDescending;
	    }
	    else
	    {
		if(cr == 1) return NSOrderedDescending;
		if(cr == -1) return NSOrderedAscending;
	    }
	    
	
	}
        else if ([[thisOne objectForKey: @"type"] intValue] == SORTCODE)
        {
            int ll;
            if([[thisOne objectForKey: @"codeLevel"] intValue]  > 0)
            {
                ll = compCodeByLevel((NSString *) [first objectForKey: [thisOne objectForKey: @"col"] ], 
                    [second objectForKey: [thisOne objectForKey: @"col"] ]);
                if(ll >= [[thisOne objectForKey: @"codeLevel"] intValue] ) continue;
            }
	    cr = [(NSString *) [first objectForKey: [thisOne objectForKey: @"col"] ] compare:
		[second objectForKey: [thisOne objectForKey: @"col"] ]];
	    if(cr == NSOrderedSame) continue;
	    if([[thisOne objectForKey: @"up"] boolValue] == YES)
	    {
		if(cr == NSOrderedAscending) return NSOrderedAscending;
		if(cr == NSOrderedDescending) return NSOrderedDescending;
	    }
	    else
	    {
		if(cr == NSOrderedAscending) return NSOrderedDescending;
		if(cr == NSOrderedDescending) return NSOrderedAscending;
	    }
	    
        
        }
   
    }
    return NSOrderedSame;

}


@implementation MWSortStackItem
-(id) init
{
    [super init];
#ifdef NSOBJVERS
    dateFormat = [[NSMutableString alloc] init];
    columnID = [[NSMutableString alloc] init];
#else
    slf = [[NSMutableDictionary alloc] init];
#endif
    return self;
}
-(id) initWithID: (NSString *) who direction: (BOOL) which type: (int) how dateFormat: (NSString *) df codeLevel: (int) cl
{
    [super init];
#ifdef NSOBJVERS
    dateFormat = [[NSMutableString alloc] initWithString: df];
    columnID = [[NSMutableString alloc] initWithString: who];
    up = which;
    codeLevel = cl;
    type = how;
#else
    [slf setObject: [NSMutableString stringWithString: df] forKey: @"dateFormat"];
    [slf setObject: [NSMutableString stringWithString: who] forKey: @"col"];
    //dateFormat = [[NSMutableString alloc] initWithString: df];
    //columnID = who;
    [slf setObject: [NSNumber numberWithBool: which] forKey: @"up"];
    [slf setObject: [NSNumber numberWithInt: how] forKey: @"type"];
    [slf setObject: [NSNumber numberWithInt: cl] forKey: @"codeLevel"];
#endif    
    return self;
}
#ifdef NSOBJVERS

-(void) encodeWithCoder: (NSCoder *) coder
{
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    //[coder encodeObject: [[columnID copy] autoRelease]];
    //[coder encodeObject: [[dateFormat copy] autorelease] ];
    
    
    [md setObject: [[columnID copy] autorelease] forKey: @"col"];
    [md setObject: [[dateFormat copy] autorelease] forKey: @"dateFormat"];
    [md setObject: [NSNumber numberWithBool: up] forKey: @"up"];
    [md setObject: [NSNumber numberWithBool: type] forKey: @"type"];
    [md setObject: [NSNumber numberWithBool: codeLevel] forKey: @"codeLevel"];
    //[super encodeWithCoder: coder];
    [coder encodeObject: md];
}
-(id) initWithCoder: (NSCoder *) coder
{
    NSDictionary *md;
    [super init];
    md = [coder decodeObject];
    dateFormat = [[NSMutableString alloc] initWithString: [md objectForKey: @"dateFormat"]];
    columnID = [[NSMutableString alloc] initWithString: [md objectForKey: @"col"]];
    up = [[md objectForKey: @"up"] boolValue];
    type = [[md objectForKey: @"type"] intValue];
    codeLevel = [[md objectForKey: @"codeLevel"] intValue];
    return self;
}
#else
-(void) encodeWithCoder: (NSCoder *) coder{

    [coder encodeObject: self];
}
-(id) initWithCoder: (NSCoder *) coder
{
    NSDictionary *md;
    [super init];
    md = [coder decodeObject];
    slf = [[NSMutableDictionary alloc] init];
    [slf addEntriesFromDictionary: md];
    return self;
}
#endif

#ifdef NSOBJVERS
-(NSString *) column {return columnID;}
-(BOOL) direction {return up;}
-(int) sortType {return type;}
-(void) setDateFormat: (NSString *) df {[dateFormat setString: df];}
-(NSString *) dateFormat {return dateFormat;}
-(int) codeLevel {return codeLevel;}
-(void) dealloc {[dateFormat dealloc]; [super dealloc];}

#else

-(NSString *) column {return [slf objectForKey: @"col"];}
-(BOOL) direction {return [[slf objectForKey: @"up"] boolValue];}
-(int) sortType {return [[slf objectForKey: @"type"] intValue];}
-(void) setDateFormat: (NSString *) df {[slf setObject: [[df copy] autorelease] forKey: @"dateFormat"];}
-(NSString *) dateFormat {return [slf objectForKey: @"dateFormat"];}
-(int) codeLevel {return [[slf objectForKey: @"codeLevel"] intValue];}
-(void) dealloc { 
#ifdef NSOBJVERS
    [dateFormat release];
    [columnID release];
#else
    [slf release];
#endif
    [super dealloc];}
#endif
@end
