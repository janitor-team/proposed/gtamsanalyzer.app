//
//  ctQChar.h
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MWFile.h"
//#import "tams.h"


@interface ctQChar : NSObject {
	int tokentype;
	unsigned long where, whend;
        unsigned line;
	NSMutableString *buff, *extra, *coder;
        MWFile *source;
}
- (id) init;
- (void) clean;
- (void) setWhere: (long) w;
- (long) where;
-(long) end;
-(void) setEnd: (long) w;
-(NSRange) range;
-(unsigned) line;
-(void) setLine: (unsigned) what;
- (void) dealloc;
- (NSMutableString *) buff;
- (NSMutableString *) extra;
- (NSMutableString *) coder;
- (int) tokentype;
- (void) setTokenType: (int) tt;
- (void) setBuff: (NSString *) ss;
-(void) addBuff: (NSString *)ss;
- (void) setExtra: (NSString *) ss;
-(void) addExtra: (NSString *) ss;
- (void) setCoder: (NSString *) ss;
-(void) setSource: (MWFile *) who;
-(MWFile *) source;
-(id) copy;
@end
