//
//  MWDoubleDictionary.m
//  TEST2
//
//  Created by matthew on Wed Aug 13 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MWDoubleDictionary.h"
#import "utils.h"

@implementation MWDoubleDictionary
-(id) init
{
    [super init];
    root = [[NSMutableDictionary alloc] init];
    return self;
}

-(void) dealloc
{
    [root release];
    [super dealloc];
}

-(NSObject *) objectForKey1: (NSString *) who1 key2: (NSString *) who2;
{
    NSMutableDictionary *first;
    
    first = [root objectForKey: who1];
    if(first == nil) return nil;
    else
        return [first objectForKey: who2];
}

-(void) setObject: (NSObject *) what forKey1: (NSString *) who1 key2: (NSString*) who2;
{
    NSMutableDictionary *two;
    
    if((two = [root objectForKey: who1]) == nil)
    {
        [root setObject: (two = [NSMutableDictionary dictionary]) forKey: who1];
    }
    [two setObject: what forKey: who2];
}
-(void) removeAllObjects
{
    [root removeAllObjects];
}
-(void) incKey1: (NSString *) who1 key2: (NSString *) who2;
{
    if([self objectExistsForKey1: who1 key2: who2] == NO)
    {
        [self setInt: 1 forKey1: who1 key2: who2];
    }
    else
    {
        [self setInt: [self intForKey1: who1 key2: who2] + 1 forKey1: who1 key2: who2];
    }
}
    
-(void) decKey1: (NSString *) who1 key2: (NSString *) who2;
{
    if([self objectExistsForKey1: who1 key2: who2] == NO)
    {
        [self setInt: -1 forKey1: who1 key2: who2];
    }
    else
    {
        [self setInt: [self intForKey1: who1 key2: who2] - 1 forKey1: who1 key2: who2];
    }
}
-(void) setInt: (int) what forKey1: (NSString *) who1 key2: (NSString *) who2;
{
    NSNumber *myNum = [NSNumber numberWithInt: what];
    [self setObject: myNum forKey1: who1 key2: who2];
}
    
-(int) intForKey1: (NSString *) who1 key2: (NSString *) who2
{
    NSNumber *what;
    what = [self objectForKey1: who1 key2: who2];
    if(what == nil) return 0;
    else return [what intValue];
}

-(BOOL) key1Exists: (NSString*) who1;
{
    if([root objectForKey: who1] == nil) return NO;
    return YES;
}

-(BOOL) objectExistsForKey1:  (NSString *) who1 key2: (NSString *) who2;
{
    if([self objectForKey1: who1 key2: who2] == nil) return NO;
    return YES;
}
-(NSArray *) firstKeys
{
    return [root allKeys];
}
-(NSArray *) keysForKey1: (NSString *) who1
{
    NSMutableDictionary *ww;
    
    ww = [root objectForKey: who1];
    if(ww == nil) return nil;
    else return [ww allKeys];
}

-(int) sumForKey1: (NSString *) who1
{
    int ans;
    NSArray *who;
    
     who = [self keysForKey1: who1];
    
    ans = 0;
    if(who != nil)
    {
        FORALL(who)
        {
            ans += [self intForKey1: who1 key2: temp];
        }
        ENDFORALL;
    }
    return ans;
}

-(int) sumForKey2: (NSString *) who2
{
    int ans;
    NSArray *who = [self firstKeys];
    
    ans = 0;
    
    if(who)
    {
        FORALL(who)
        {
            ans += [self intForKey1: temp key2: who2];
        }
        ENDFORALL;
    }
    return ans;
}

-(int) diagSum
{
    int ans;
    NSArray *who = [self firstKeys];
    
    ans = 0;
    
    if(who)
    {
        FORALL(who)
        {
            ans += [self intForKey1: temp key2: temp];
        }
        ENDFORALL;
    }
    return ans;
}

-(unsigned) count
{
    NSArray *fk = [self firstKeys];
    unsigned i;
    
    i = 0;
    FORALL(fk)
    {
        i+=[[self keysForKey1: temp] count];
    }
    ENDFORALL;
    return i;
}

-(NSArray *) toArray
{
    int i,n;
    NSArray *a1;
    NSMutableArray *aa;
    NSMutableDictionary *dd;
    NSArray *fk = [self firstKeys];
    
    aa = [[NSMutableArray alloc] init];
    FORALL(fk);
    {
        a1 = [self keysForKey1: temp];
        n = [a1 count];
        for(i = 0; i < n; i++)
        {
            dd = [NSMutableDictionary dictionary];
            [dd setObject: temp forKey: @"key1"];
            [dd setObject: [a1 objectAtIndex: i] forKey: @"key2"];
            [dd setObject: [self objectForKey1: temp key2:
                [a1 objectAtIndex: i]] forKey: @"object"];
            [aa addObject: dd];
            
        }
    }
    ENDFORALL;
    [aa autorelease];
    return aa;
}
-(void) removeObjectForKey1: (NSString*) who1 key2: (NSString*) who2
{
    NSMutableDictionary *who;
    who = [root objectForKey: who1];
    [who removeObjectForKey: who2];
    if([who count] == 0) [root removeObjectForKey: who1];
}
@end
