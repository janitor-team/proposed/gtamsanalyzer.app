//
//  utils.m
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "utils.h"
int menuStringComp(id first, id second, void *key)
{
    return [[first lowercaseString] compare: [second lowercaseString]];//[first caseInsensitiveCompare: second];
}

NSString *charToNSS(char c)
{
    NSString *tmp;
    char ss[2];
    ss[0] = c;
    ss[1] = '\0';
    tmp = [NSString stringWithCString: ss];
    //[tmp retain];
    return tmp;
}
NSMutableString *trimNSS(NSMutableString *s)
{
    char c;
    //keep checking the first character
    for(;;)
    {
	if([s length] == 0) break;
	c = [s characterAtIndex: 0];
	if(c == ' ' || c == '\t' || c == '\n' || c == '\r')
	    [s deleteCharactersInRange: NSMakeRange(0, 1)];
	else
	    break;
    }
    for(;;)
    {
	if([s length] == 0) break;
	c = [s characterAtIndex: ([s length] - 1)];
	if(c == ' ' || c == '\t' || c == '\n' || c == '\r')
	    [s deleteCharactersInRange: NSMakeRange([s length] - 1, 1)];
	else
	    break;
    }
    return s;
    
}
NSMutableString *trimCharNSS(NSMutableString *s, unichar ch)
{
    unichar c;
    //keep checking the first character
    for(;;)
    {
	if([s length] == 0) break;
	c = [s characterAtIndex: 0];
	if(c == ch)
	    [s deleteCharactersInRange: NSMakeRange(0, 1)];
	else
	    break;
    }
    for(;;)
    {
	if([s length] == 0) break;
	c = [s characterAtIndex: ([s length] - 1)];
	if(c == ch)
	    [s deleteCharactersInRange: NSMakeRange([s length] - 1, 1)];
	else
	    break;
    }
    return s;
    
}

NSMutableArray *convertStringToArray(NSString *theString, char breakChar)
{
    int i, len;
    int inString;
    char c;
    NSMutableString *ss = [[NSMutableString alloc] init];
    NSMutableArray *ans = [[NSMutableArray alloc] init];
    len = [theString length];
    [ss setString: @""];
    inString = 0;
    for(i = 0; i < len; i++)
    {
	c = [theString characterAtIndex: i];
	if(c == '\"' && !inString)
	{
	    [ss ADDCHAR(c)];
	    inString = 1;
	}
	else if(inString)
	{
	    [ss ADDCHAR(c)];
	    if(c == '\"') inString = 0;
	}
	else if (c == breakChar)
	{
	    if([ss length] > 0)
	    {
		[ans addObject: [ss copy]];
		[ss setString: @""];
	    }
	}
	else
	{
	    [ss ADDCHAR(c)];
	}
    }
    if([ss length])
    {
	[ans addObject: [ss copy]];
    }
    [ss release];
    return [ans autorelease];
}

BOOL addUniqueToArray(NSMutableArray *who, NSString *what)
{
    int cnt;
    int i;
    cnt = [who count];
    for(i = 0; i < cnt; i++)
    {
        if([[who objectAtIndex: i] isEqualToString: what] == YES) return NO;
    }
    [who addObject: what];
    return YES;
}
int nsSubString(NSString *first, NSString *sec, BOOL caseInsensitive)
{
    NSRange rng;
    NSRange nulRange = {NSNotFound, 0};
    
    
    if(caseInsensitive == YES)
    {
	if((rng = [first rangeOfString: sec options: caseInsensitive]).location  == NSNotFound)
	    return -1;
	else
	    return rng.location;
    }
    else
    {
	if((rng = [first rangeOfString: sec ]).location  == NSNotFound)
	    return -1;
	else
	    return rng.location;
    }
}
 
	


