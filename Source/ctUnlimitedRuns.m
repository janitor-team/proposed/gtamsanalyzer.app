//
//  ctUnlimitedRuns.m
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "utils.h"
#import "ctUnlimitedRuns.h"
#import "ctReadChar.h"
#import "coderec.h"
#import "myResults.h"
#import "ctLimitFunc.h"
#import "prefBoss.h"
#define PRINTCODER(X, Y) {if([gPrefBoss showCoder] && [(NSString *)Y length]){[X appendString:@" ["]; [X appendString: Y]; [X appendString: @"]"];}}

NSMutableArray *ctRunVault;


void ctInitRV()
{
    if(ctRunVault)
    {
	if(ctRunVault)
	    [ctRunVault release];
	//we may need to get rid of this stuff
	// [ctRunVault removeAllObjects];
    }
    ctRunVault = [[NSMutableArray alloc] init];
    [ctRunVault retain];
}

void ctAddOpenChar(ctQChar *qq)
{
	FORALL(ctRunVault)
	{
	    if(([temp openRun] == YES) /*&& !isrepeat(qq)*/)
	    {
		switch([qq tokentype])
		{
		    case CHAR:
			[(coderec *)temp addString: [qq buff]];
			break;
		    case TOKEN:
			if(israw() && ( [gCurrentTAMS isRepeat: [temp theName]] == NO))
			{
			    [(coderec *)temp addString: @"{"];
			    [(coderec *)temp addString: [qq buff]];
			    //add coder
			    PRINTCODER((coderec *)temp, [qq coder]);
			    [(coderec *)temp addString: @"}"];
			}
			break;
		    case ENDTOKEN:
			if(israw() && ( [gCurrentTAMS isRepeat: [temp theName]] == NO))
			{
			    [(coderec *)temp addString: @"{/"];
			    [(coderec *)temp addString: [qq buff]];
			    //add coder
			    PRINTCODER((coderec *)temp, [qq coder]);
			    [(coderec *)temp addString: @"}"];
			}
		    
		    default:;
		};
			
			    
	    }
	}
	ENDFORALL;
}

int isrepeat (ctQChar *qq)
{
    if(gCurrentTAMS)
	return [gCurrentTAMS isRepeat: [qq buff]];
    else
	return 0;
}

void ctOpenRun(ctQChar *qq)
{
    coderec *cr;
    cr = [[coderec alloc] init];
     if(israw() && !isrepeat(qq))
    {
	
	FORALL(ctZoneVault)
	{
	    [[cr record] appendString: @"{"];
	    [[cr record] appendString: [temp objectForKey: @"codeName"]];
	    //add coder
	    PRINTCODER([cr record], [temp objectForKey:@"coderName"]);
	    [[cr record] appendString: @"}"];
	}ENDFORALL;
    }
    
   [cr setName: [qq buff]];
    [cr setBegin: [qq where]];
    [cr setCoder: [qq coder]];
    [cr setDoc: hotSearchDocument];
    [cr setOpen: YES];
    [cr retain];
     if(isrepeat(qq)) [cr setRunType: REPEATRUN];
   [ctRunVault addObject: cr];
    //need to add current state at this point!
    //need to add self tothe current state
}
void ctCloseAllOpenRuns()
{
   FORALL(ctRunVault)
    {
	if([temp openRun] == YES)
	{
	    if(israw())
	    {
		[[temp record] appendString: @"{/EOF}"];
	    }
	    [temp setOpen: NO];
	    [temp setComment: @"End of file found without close tag"];
	    [temp setEnd: getCharLocation()];
	}
    
    }
    ENDFORALL;
    //need to check out of the current state


}
void ctCloseRun(ctQChar *qq)
{
    FORALL(ctRunVault)
    {
	if(([[(coderec *)temp theName] isEqualToString: [qq buff]] == YES) &&( [temp openRun] == YES) && ([[(coderec *) temp theCoder] isEqualToString: [qq coder]])) //add coder here too
	{
	    if(israw() && ( [gCurrentTAMS isRepeat: [temp theName]] == NO))
	    {
		[[temp record] appendString: @"{/"];
		[[temp record] appendString: [qq buff]];
		//add coder
		PRINTCODER([temp record], [qq coder]);
		[[temp record] appendString: @"}"];
	    }
	    [temp setOpen: NO];
	    [temp setComment: [qq extra]];
	    [temp setEnd: [qq where]];
	    return;
	}
    
    }
    ENDFORALL;
    //need to check out of the current state
}

int israw()
{
    if (gCurrentTAMS)
	return [gCurrentTAMS global: @"rawflag"];
    else return 0;
}
