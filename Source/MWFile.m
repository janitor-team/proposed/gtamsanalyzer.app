//
//  MWFile.m
//  TA2 hot
//
//  Created by matthew on Sat Apr 05 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "MWFile.h"
#import "MWFakeDoc.h"

@implementation MWFile


- (NSString *) path 
{
    if(fid != nil)
        if([fid isReal] == NO) return path;
    
    if(open) 
    {
        if([fid fileName])
            return [fid fileName]; 
        else
            return @"";
    }
    else 
        return path;
}
- (NSString *) name {
	if([self hasFile]) return [[self path] lastPathComponent];
	else if([self open]) return [fid getTitle];
	else return @"";

/*
    if(open)
        if([fid isReal] == NO) return [path lastPathComponent];
     if([fid fileName])

            return [path lastPathComponent];
	else if(open) return [fid getTitle];
	return @"";
*/
}
- (NSString *) type {return [path pathExtension];}
-(NSAttributedString *) attributedString
{
    NSDictionary *myAttrib;

    NSAttributedString *masterString;
    /*
    if([[self type] isEqualToString: @"rtf"]
            || [[self type] isEqualToString: @"RTF"] || [[self type] isEqualToString: @"RTFD"]
            || [[self type] isEqualToString: @"rtfd"]  )
    */
    if([self hasFile])
    {
        return [[[NSAttributedString alloc] initWithPath: [self path] documentAttributes: &myAttrib] autorelease];
    }
}
- (NSString *) string
{
    NSString *myString;
    NSAttributedString *masterString;
    NSDictionary *myAttrib;
    if([self open])
    {
       return  [[[fid getData] copy] autorelease];
    }
    else //may need to deal with strings...
    {
        if([[self type] isEqualToString: @"txt"]
            || [[self type] isEqualToString: @"TXT"])
        {
            return [[NSString alloc] initWithContentsOfFile: [self path]];
        }
        else if([[self type] isEqualToString: @"rtf"]
            || [[self type] isEqualToString: @"RTF"] || [[self type] isEqualToString: @"RTFD"]
            || [[self type] isEqualToString: @"rtfd"]  )
        {
            masterString = [[NSAttributedString alloc] initWithPath: [self path] documentAttributes: &myAttrib];
            myString = [[NSString alloc] initWithString: [masterString string]];
            [masterString release];
            return [myString autorelease] ;
        }
    }
}

-(void) setPath: (NSString *) aPath
{
    [path setString: aPath];
//    [self setHasFile: YES];
}
- (id) filePointer
{
    return fid; //may need to open a file also
}
- (BOOL) open {return open;}
-(BOOL) isReallyOpen
{
    if(open && fid != nil)
    {
        if([fid isReal])
            return YES;
    }
    return NO;
}

-(void) setOpen: (BOOL) state {open = state; if(state == NO) fid = nil;}
-(id) init
{
    [super init];
    path = [[NSMutableString alloc] init];
    open = NO;
    fid = nil;
    [self setHasFile: NO];
    return self;
}

-(id) initWithPath: (NSString *) aPath
{
    id who = [self init];
    if (who)
        [path setString: aPath];
    open = NO;
    [self setHasFile: YES];
    return who;
}
-(void) dealloc
{
    [path release];
    [super dealloc];
}
-(id) setOpenFakeFileWithWorkBench: (id) wb
{
    if(fid != nil) return nil;
    fid = [[MWFakeDoc alloc] initWithMWFile: self workBench: wb];
    if (fid == nil) return fid;
    [self setHasFile: YES];
    open = YES;
    [fid open];
    
}
-(void) closeFakeFile
{
    if(fid == nil) return;
    if([fid isReal] == YES) return;
    [fid writeAndClose];
    [fid release];
    fid = nil;
    open = NO;
}
-(void) setOpenFile: (id) aFid
{
    NSString *thePath;
    fid = aFid;
    if(aFid != nil) open = YES; else open = NO;
    if(aFid) 
    {
        thePath = [aFid fileName];
        if(thePath)
        {
            [self setPath: thePath];
            [self setHasFile: YES];
        }
        else
            [self setPath: [aFid getTitle]];
    }
}
-(void) setHasFile: (BOOL) state {hasFile = state;}
-(BOOL) hasFile 
{
    if(fid)
    {
        if([fid fileName])
        {
            [self setPath: [fid fileName]];
            return YES;
        }
        else
            return NO;
    }
    else
    return hasFile;
}


-(BOOL) isEqual: (MWFile *) who
{
    return [[self path] isEqualToString: [who path]];
}
-(BOOL) compare: (MWFile *) who
{
    return [[self path] compare: [who path]];
}

@end
