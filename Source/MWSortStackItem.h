//
//  MWSortStackItem.h
//  TamsAnalyzer
//
//  Created by matthew on Sat Mar 08 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#define SORTDATE 1
#define SORTSTRING 2
#define SORTFLOATNUMBER 3
#define SORTINTNUMBER 4
#define SORTCODE 5
#define NSOBJVERS


#import <Foundation/Foundation.h>
int stackComp(id first, id second, NSMutableArray *sortStack);

@interface MWSortStackItem 
#ifdef NSOBJVERS
: NSObject {
#else
: NSObject {
#endif
#ifdef NSOBJVERS
    NSString *columnID;
    BOOL up;
    int type;
    NSMutableString *dateFormat;
    int codeLevel;
#else
    NSMutableDictionary *slf;
#endif
}
-(id) init;
-(id) initWithID: (NSString *) who direction: (BOOL) which type: (int) how dateFormat: (NSString *) df codeLevel: (int)cl;
-(NSString *) column;
-(BOOL) direction;
-(int) sortType;
-(void) setDateFormat: (NSString *) df;
-(NSString *) dateFormat;
-(int) codeLevel;
#ifdef NSOBJVERS
//-(void) setObject: (id) what forKey: (id) who;
#endif
@end
