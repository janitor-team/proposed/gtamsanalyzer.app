//
//  MGWMyDocumentToolbar.h
//  avtams
//
//  Created by matthew on Tue Mar 09 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MGWMyDocumentToolbar : NSObject {
    IBOutlet id theDocument;
    IBOutlet id mainWindow;
    int buttonNdx;
    NSMutableArray *myButtons;
    NSMutableArray *allButtons;
    NSToolbar *myToolBar;    
    
}
- (void)setupToolbar;

- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
     itemForItemIdentifier:(NSString *)itemIdentifier
 willBeInsertedIntoToolbar:(BOOL)flag;
- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar;
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar;
-(BOOL) isButton: (NSString *) who;
-(void) clearButtons;
-(NSToolbarItem *) itemWithID: (NSString *) itemID image: (NSString *) fn action: (SEL) act;

@end
