/* docDelegate */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#define TAMSDocDidChange @"TAMSDocDidChange"
/*
Delegate for both the NSText and the doc
*/
@interface docDelegate : NSObject
{
    IBOutlet id myBoss;
}

- (void)windowWillClose:(NSNotification *)aNotification;
- (void)textDidChange:(NSNotification *)aNotification;
@end
