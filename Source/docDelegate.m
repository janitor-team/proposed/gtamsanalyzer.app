#import "docDelegate.h"
#import "myProject.h"
#import "MyDocument.h"
#define NOTIFICATIONDIRTY 1
#import "tams.h"
//#define jagDBG 1

@implementation docDelegate
- (void)windowWillClose:(NSNotification *)aNotification
{
    MWFile *mw;
    
    mw = [myBoss getMyMWFile];
    if(mw)
    {
        [[NSNotificationCenter defaultCenter] removeObserver: myBoss name: @"TAGoReanalysisMode" object: nil];
        [mw setOpenFile: myBoss]; //reset the path
        [mw setOpenFile: nil]; //close it
        [mw setOpen: NO];
        if([mw hasFile] == NO)
        {
            [[myBoss getGWorkBench] dropMWFile: mw];
        }
    }
    [myBoss endTimer];
/*
    if([myBoss getGWorkBench])
    {
	[[myBoss getGWorkBench] unregisterWindow: [[[aNotification object] windowController] owner]];
#ifdef jagDBG
	//jaguar debug:
	[[[[aNotification object] windowController] owner] close];
#endif
    }
*/	
}
- (void)textDidChange:(NSNotification *)aNotification
{
#ifdef TEXTKLUDGE
    if([myBoss isDocumentEdited] == NO) [myBoss updateChangeCount: NSChangeDone];
#endif

#ifdef NOTIFICATIONDIRTY
    [[NSNotificationCenter defaultCenter] postNotificationName: TAMSDocDidChange object: [myBoss getMyMWFile]];
#else
    [myBoss setDirty: YES];
#endif
}
- (BOOL)textShouldBeginEditing:(NSText *)aTextObject
{
    return YES;
}
- (void)windowDidBecomeKey:(NSNotification *)aNotification
{
    if([myBoss getGWorkBench] == nil) return;
    [myBoss rebuildDocMacros: nil];
    [myBoss renewBookmarkMenu];
    [[myBoss getGWorkBench] rebuildAutoSetMenu];
    [[myBoss getGWorkBench] rebuildCodeSetMenu];
    [[myBoss getGWorkBench] rebuildSummReportMenu];
    if([[myBoss getGWorkBench] reanalysisMode] == YES)
    {
        [[[NSApp delegate] reanalysisMenu] setState: NSOnState];
    }
    else
    {
        [[[NSApp delegate] reanalysisMenu] setState: NSOffState];
    }
    

}


@end
