#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>


#import "AGRegex.h"


@interface AGRegex (NSEscapeReplace)
- (NSString *)replaceWithString:(NSString *)rep inString:(NSString *)str limit:(int)lim range: (NSRange) r;

- (NSString *)replaceWithStringWithEscape:(NSString *)rep inString:(NSString *)str  raw: (int) rawflag;
- (NSString *)replaceWithStringWithEscape:(NSString *)rep inString:(NSString *)str limit: (int) lim range: (NSRange) r raw: (int) rawflag;

@end
@interface NSMutableAttributedString(MGWAdditions)
-(void) setColor: (NSColor *) myC;
-(void) appendString: (NSString *) who;
-(void) setString: (NSString *) who;

@end

@interface NSString (NSStringTextFinding)
- (NSRange)findString:(NSString *)string selectedRange:(NSRange)selectedRange options:(unsigned)options wrap:(BOOL)wrap regex: (BOOL) regexFlag multiline: (BOOL) mlFlag substring: (int) sbstr;

- (NSRange)findString:(NSString *)string selectedRange:(NSRange)selectedRange options:(unsigned)mask wrap:(BOOL)wrapFlag regex: (BOOL) regexFlag multiline: (BOOL) mlFlag;
-(NSString *) stringUnescaped;
-(BOOL) startsWith: (NSString *) who;

@end
