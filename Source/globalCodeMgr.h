/* globalCodeMgr */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface globalCodeMgr : NSObject
{
    IBOutlet id codeView;
    IBOutlet id myProj;
}
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
- (int)numberOfRowsInTableView:(NSTableView *)aTableView;
- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
@end
