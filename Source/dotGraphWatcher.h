/* dotGraphWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "MWDoubleDictionary.h"
@interface dotGraphWatcher : NSObject
{
    IBOutlet id arrowDirMenu;
    IBOutlet id nBorderMenu;
    IBOutlet id nShapeMenu;
    IBOutlet id nGroupMenu;
    IBOutlet id dgMenu;
    IBOutlet id fieldList;
    IBOutlet id levelMenu;
    IBOutlet id lineTypeMenu;
    IBOutlet id nameField;
    IBOutlet id theBoss;
    IBOutlet id thePane;
    IBOutlet id typeMenu;
    IBOutlet id saveButton;
    IBOutlet id attribTable;
    
    IBOutlet id edgeAttribTable;
    IBOutlet id fromMenu;
    IBOutlet id toMenu;
    IBOutlet id ftStyleMenu;
    IBOutlet id ftDirMenu;
    IBOutlet id ftTypeMenu;
    IBOutlet id countSwitch;
    IBOutlet id codeLevelSwitch;
    //----- Specific node 
    IBOutlet id snVarMenu;
    IBOutlet id snValMenu;
    IBOutlet id snShapeMenu;
    IBOutlet id snStyleMenu;
    IBOutlet id snTable;
    NSMutableDictionary *snDict;
    
    //----- Specific edges
    IBOutlet id seVarMenu;
    IBOutlet id seValMenu;
    IBOutlet id seShapeMenu;
    IBOutlet id seStyleMenu;
    IBOutlet id seTable;
    MWDoubleDictionary *seDict;
    IBOutlet id seDirMenu;
    
    IBOutlet id overrideSwitch;
    IBOutlet id codeLevel;
    NSMutableArray *colList;
    NSMutableArray *dndList;
    int dndNdx;
    MWDoubleDictionary *pairCount;
    MWDoubleDictionary *pairAttribs;
    MWDoubleDictionary *edgeAttribs;
    NSMutableArray *pairList;
    NSMutableArray *colPool;
    NSMutableArray *attribs;
    BOOL graphvizInstalled, saveFlag;

}
- (void) doDotGraph;
- (IBAction)addDotGraph:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)delAllDotGraph:(id)sender;
- (IBAction)delDotGraph:(id)sender;
- (IBAction)writeDotGraph:(id)sender;
-(void) doDotGraph;
-(IBAction) addAttrib:(id)sender;
-(IBAction) delAttrib: (id) sender;
-(IBAction) delAllAttrib: (id) sender;
-(NSString *) attribNdx: (int) rowIndex;
-(NSString *) findAttrib: (NSString *) who forWhat: (NSString *) what;
-(IBAction) addEdgeAttrib: (id)sender;
-(IBAction) delEdgeAttrib: (id) sender;
-(IBAction) delAllEdgeAttrib: (id) sender;
//-(NSString *) edgeAttribsForFrom: (NSString *) from To: (NSString *) to;
-(void) openGraphviz: (NSString *) theName;

-(IBAction) snValHandler: (id) sender;
-(IBAction) snAdd: (id) sender;
-(IBAction) snDel: (id) sender;
-(IBAction) snDelAll: (id) sender;

-(IBAction) seValHandler: (id) sender;
-(IBAction) seAdd: (id) sender;
-(IBAction) seDel: (id) sender;
-(IBAction) seDelAll: (id) sender;
-(IBAction) override:(id) sender;

-(int) codeLevel;

@end
