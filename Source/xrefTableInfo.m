//
//  xrefTableInfo.m
//  TamsAnalyzer
//
//  Created by matthew on Sun Jun 16 2002.
//  Copyright (c) 2002 Matthew Weinstein. All rights reserved.
//

#import "xrefTableInfo.h"


@implementation xrefTableInfo
-(id) init
{
    [super init];
    name = [[NSMutableString alloc] init];
    cnt = 0;
    return self;
}

-(int) cnt{return cnt;}
-(NSMutableString *) theName{return name;}
-(void) setTheName: (NSString *) n
{
    [name setString: n];
}
-(void) inc
{cnt++;}
-(void) setCnt: (int) c
{
    cnt = c;
}

@end
