/* setMacroWatcher */

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface setMacroWatcher : NSObject
{
    IBOutlet id fpOption;
    IBOutlet id macroMenu;
    IBOutlet id macroView;
    IBOutlet id nameView;
    IBOutlet id regexOption;
    IBOutlet id columnMenu;
    IBOutlet id selectModeMenu;
    IBOutlet id selectStringView;
    IBOutlet id selectTypeMenu;
    IBOutlet id setObjectMenu;
    IBOutlet id localAutosetMenu;
    IBOutlet id setOpMenu;
    IBOutlet id startMenu;
    IBOutlet id globalScope;
    IBOutlet id sortSwitch;
    IBOutlet id codeSetMenu;
    IBOutlet id codeSetSelMenu;
    IBOutlet id codeSetExact;
    IBOutlet id myOverLord;
    IBOutlet id macroSheet;
    NSMutableDictionary *currMacro;
}
/* initialization routines */
-(IBAction) zeroEditor: (id) sender;

//-(void) setEditorFor: (NSString *) who;
-(IBAction) addAutosetStep: (id) sender;
-(IBAction) addAutoHistory: (id) sender;
- (IBAction)addSelectStep:(id)sender;
- (IBAction)addSetStep:(id)sender;
-(IBAction) addCodeSetStep: (id) sender;
- (IBAction)cancelMacro:(id)sender;
- (IBAction)deleteAllSteps:(id)sender;
- (IBAction)deleteStep:(id)sender;
- (IBAction) deleteMacro: (id) sender;
- (IBAction)loadMacro:(id)sender;
- (IBAction)moveDown:(id)sender;
- (IBAction)moveUp:(id)sender;
- (IBAction)saveMacro:(id)sender;
- (IBAction)runMacro:(id)sender;

-(void) runMacroEditor;

@end
