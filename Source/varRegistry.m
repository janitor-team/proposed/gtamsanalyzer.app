//
//  varRegistry.m
//  TAMS Edit
//
//  Created by matthew on Sun Apr 21 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "varRegistry.h"


@implementation varRegistry
-(id) init
{
    [super init];
    theVault = [[NSMutableDictionary alloc] init];
    return self;
}
-(NSMutableDictionary *) getGlobals 
{return theVault;}
-(void) setGlobals: (NSDictionary *) who 
{[theVault addEntriesFromDictionary: who];}
-(NSNumber *) find: (NSString*) key
{
    return [theVault objectForKey: key];
}
-(int) findInt: (NSString*) key
{
    return [[theVault objectForKey: key] intValue];
}
-(unsigned) findUns: (NSString* ) key
{
    return [[theVault objectForKey: key] unsignedIntValue];
}

-(void) setKey: (NSString *) key intValue: (int) myValue
{
    [theVault setObject: [NSNumber numberWithInt: myValue] forKey: key];
}
-(void) setKey: (NSString *) key unsValue: (unsigned) myValue
{
    [theVault setObject: [NSNumber numberWithUnsignedInt: myValue] forKey: key];
}
@end
