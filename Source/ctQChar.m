//
//  ctQChar.m
//  CocoaTams
//
//  Created by matthew on Mon Apr 15 2002.
//  Copyright (c) 2001 Matthew Weinstein. All rights reserved.
//

#import "ctQChar.h"


@implementation ctQChar
- (id) init
{
    [super init];
    buff = [[NSMutableString alloc] init];
    extra = [[NSMutableString alloc] init] ;
    coder = [[NSMutableString alloc] init] ;
    source = nil;
    line = 0;
    whend=where=0L;
    return self;
}
-(void) clean
{
    [buff setString: @""];
    [extra setString: @""];
    [coder setString: @""];
    line = 0;
    source = nil;
    line = 0;
    whend=where = 0L;
    source = nil;
}
- (void) setWhere:(long) w
{
    where = w;
}
- (long) where {return where;}
- (void) dealloc;
{
    [buff release];
    [extra release];
    [coder release];
    [super dealloc];
}

- (NSMutableString *) buff
{
    return buff;
}

- (NSMutableString *) extra;
{ return extra;}

- (NSMutableString *) coder;
{ return coder;}

- (int) tokentype;
{return tokentype;}
-(void) setEnd: (long) w
{
    whend = w;
}
-(long) end
{
    return whend;
}

-(NSRange) range
{
    NSRange r;
    r.location = where;
    r.length = whend - where;
    return r;
}

- (void) setTokenType: (int) tt;
{ tokentype = tt;}

- (void) setBuff: (NSString *) ss;
{
    [buff setString: ss];
}

-(void) addBuff: (NSString *) ss
{
    [buff appendString: ss];
}
- (void) setCoder: (NSString *) ss;
{
    [coder setString: ss];
}
- (void) setExtra: (NSString *) ss;
{
    if([ss length] == 0) return;
    [extra setString: ss];
}
- (void) addExtra: (NSString *) ss;
{
    [extra appendString: ss];
}
-(void) setSource: (MWFile *) who
{
    source = who;
}
-(MWFile *) source {return source;}

-(unsigned) line{ return line;}
-(void) setLine: (unsigned) what {line = what;}

- (id) copy
{
    id mycopy = [[ctQChar alloc] init];
    [mycopy setTokenType: [self tokentype]];
    [mycopy setBuff: [self buff]];
    [mycopy setCoder: [self coder]];
    [mycopy setExtra: [self extra]];
    [mycopy setSource: [self source]];
    [mycopy setWhere: [self where]];
    [mycopy setLine: [self line]];
    return mycopy;
}

@end
